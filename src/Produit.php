<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Produit {

    private $idProduit;
    private $libelle;
    private $description;
    private $prix;
    private $image;
    private $categorie;
    private $marque;

    public function getIdProduit() {
        return $this->idProduit;
    }

    private static function arrayToProduit(Array $array, Categorie $categorie=null, Marque $marque=null) {
        $p = new Produit();
        $p->idProduit = $array["idProduit"];
        $p->libelle = $array["libelle"];
        $p->description = $array["description"];
        $p->image = $array["image"];
        $p->prix = $array["prix"];
        $p->image = $array["image"];
        /* $idCategorie = $array["idCategorie"];
          if($idCategorie != null){
          $p->categorie = Categorie::fetch($array["idCategorie"]);
          } */
        if ($categorie == null) {
            if ($array["idCategorie"] != null) {
                $p->categorie = Categorie::fetch($array["idCategorie"]);
            } else {
                $p->categorie = null;
            }
        }else{
            $p->categorie = $categorie;
        }
       
        //Si la marque est nulle?
        if ($marque == null) {
            //Si l'id de la marque n'est pas nul
            if ($array["idMarque"] != null) {
                //On associe l'id de la marque au produit
                $p->marque = Marque::fetch($array["idMarque"]);
            } else {
                //on ne met rien dans la marque
                $p->marque = null;
            }
        } else {
            //si elle n'est pas nulle
            $p->marque = $marque;
        }

        return $p;
    }

    private static $select = "select * from produit";
    private static $selectById = "select * from produit where idProduit =:idProduit";
    private static $insert = "insert into produit(libelle, description, image, prix, idCategorie) values (:libelle, :description, :image, :prix, :idCategorie)";
    private static $update = "update produit set libelle=:libelle, description=:description, image=:image, prix=:prix, idCategorie=:idCategorie where idProduit =:idProduit";
    private static $delete = "delete from produit where idProduit =:idProduit";
    private static $selectByIdCategorie = "select * from produit where idCategorie=:idCategorie";
    private static $selectByIdMarque = "select * from produit where idMarque=:idMarque";


    public static function fetchAll() {
        $collectionProduit = null;
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->query(Produit::$select);
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionProduit[] = Produit::arrayToProduit($record);
        }
        return $collectionProduit;
    }

    public static function fetch($idProduit) {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$selectById);
        $pdoStatement->bindParam(":idProduit", $idProduit);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $produit = Produit::arrayToProduit($record);
        return $produit;
    }

    public function save() {
        if ($this->idProduit == null) {
            $this->insert();
        } else {
            $this->update();
        }
    }

    public function insert() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$insert);
        //$pdoStatement->bindParam(":idProduit", $this->idProduit); l'idProduit est en auto_incrément donc il ne faut pas l'ajouter manuellement
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":description", $this->description);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->bindParam(":prix", $this->prix);
        if ($this->categorie != null) {
            $idCategorie = $this->categorie->getIdCategorie();
        }
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
        $this->idProduit = $pdo->lastInsertId();
    }

    public function update() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$update);
        $pdoStatement->bindParam(":idProduit", $this->idProduit);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":description", $this->description);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->bindParam(":prix", $this->prix);
        if ($this->categorie != null) {
            $idCategorie = $this->categorie->getIdCategorie();
        }
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
    }

    public function delete() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$delete);
        $pdoStatement->bindParam(":idProduit", $this->idProduit);
        $resultat = $pdoStatement->execute();
        $nbLignesAffectees = $pdoStatement > rowCount();
        if ($nbLignesAffectees == 1) {
            $this->getCategorie()->removeProduit($this);
            $this->getMarque()->removeProduit($this);
            $this->idProduit = null;
        }
        return $resultat;
    }

    public function setCategorie(Categorie $categorie = null) {
        $this->categorie = $categorie;
        if ($categorie != null) {
            $categorie->addProduit($this);
        }
    }

    public static function fetchAllByCategorie(Categorie $categorie) {
      
        $idCategorie = $categorie->getIdCategorie();
        $collectionProduit = array();
        $pdo = (new DBA())->getPDO();
        $pdoSatement = $pdo->prepare(Produit::$selectByIdCategorie);
        $pdoSatement->bindParam(":idCategorie", $idCategorie);
        $pdoSatement->execute();
        $recordset = $pdoSatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordset as $record) {
            $collectionProduit[] = Produit::arrayToProduit($record, $categorie, $marque=null);
        }
        return $collectionProduit;
    }

    public static function fetchAllByMarque(Marque $marque) {
      
        $idMarque = $marque->getIdMarque();
        $collectionProduits = array();
        $pdo = (new DBA())->getPDO();
        $pdoSatement = $pdo->prepare(Produit::$selectByIdMarque);
        $pdoSatement->bindParam(":idMarque", $idMarque);
        $pdoSatement->execute();
        $recordset = $pdoSatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordset as $record) {
            $collectionProduits[] = Produit::arrayToProduit($record, $categorie=null, $marque);
        }
        return $collectionProduits;
    }

   

    public function compareTo(Produit $produit) {
        return $produit->idProduit == $this->idProduit;
    }

    public function getCategorie() {
        return $this->categorie;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getPrix() {
        return $this->prix;
    }

    public function getImage() {
        return $this->image;
    }

    public function setIdProduit($idProduit) {
        $this->idProduit = $idProduit;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setPrix($prix) {
        $this->prix = $prix;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function getMarque() {
        return $this->marque;
    }

    public function setMarque($marque) {
        $this->marque = $marque;
        if ($marque != null) {
            $marque->addProduit($this);
        }
    }

}
